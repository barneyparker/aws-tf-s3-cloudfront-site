variable "certificate_arn" {
  type = "string"
  description = "ARN of us-west-1 based ACM certificate"
}

variable "bucket_name" {
  type = "string"
  description = "Name for the S3 origin bucket"
}

variable "aliases" {
  type = "list"
  description = "List of domain aliases for this distribution"
}
